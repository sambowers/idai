from osgeo import gdal
import numpy as np

startdate = '20190228'
enddate = '20190302'

# Get filenames
infile_VH = '/home/sbowers3/SMFM/idai/DATA/%s_%s_min_VH_R20m.tif'%(startdate,enddate)
infile_VV = '/home/sbowers3/SMFM/idai/DATA/%s_%s_min_VV_R20m.tif'%(startdate,enddate)
outfile = '/home/sbowers3/SMFM/idai/DATA/flood_prob_%s_%s.tif'%(startdate,enddate)

# Load files
ds = gdal.Open(infile_VV,0)#'S1_output_min_VH_R20m.tif',0)
data_VV = ds.ReadAsArray()

ds = gdal.Open(infile_VH,0)#'S1_output_min_VH_R20m.tif',0)
data_VH = ds.ReadAsArray()

high_prob = data_VH <= -20
med_prob = np.logical_and(data_VH <= -17.5, data_VH > -20)
low_prob = np.logical_and(data_VH <= -15, data_VH > -17.5)
flood_prob = high_prob + (med_prob*2) + (low_prob*3)

mask = data_VH == 0.

high_prob = np.ma.array(high_prob, mask = mask)
med_prob = np.ma.array(med_prob, mask = mask)
low_prob = np.ma.array(low_prob, mask = mask)
flood_prob = np.ma.array(flood_prob, mask = mask)

def outputGeoTiff(data, outname, ds, nodata=99):
    '''
    '''
    
    driver = gdal.GetDriverByName('GTiff')
    
    ds_out = driver.Create(outname, data.shape[1], data.shape[0], 1, 1, options = ['COMPRESS=LZW'])
    
    ds_out.SetGeoTransform(ds.GetGeoTransform())
    ds_out.SetProjection(ds.GetProjection())
    
    ds_out.GetRasterBand(1).SetNoDataValue(nodata)
    ds_out.GetRasterBand(1).WriteArray(data.filled(nodata))
    ds_out = None

#outputGeoTiff(high_prob, 'high_prob_flood.tif', ds)
#outputGeoTiff(med_prob, 'med_prob_flood.tif', ds)
#outputGeoTiff(low_prob, 'low_prob_flood.tif', ds)
outputGeoTiff(flood_prob, outfile, ds, nodata = 99)

# re-cast data to integers
def recast(data, infile, ds, mask):
    '''
    '''
    
    data_int = data + 40.
    data_int[data_int<0] = 0
    data_int = data_int * 6
    data_int[data_int > 255.] = 255
    data_int = np.round(data_int, 0).astype(np.int)
    
    data_int = np.ma.array(data_int, mask = mask)
    
    outputGeoTiff(data_int, infile[:-4] + '_byte.tif', ds, nodata = 255)

recast(data_VV, infile_VV, ds, mask)
recast(data_VH, infile_VH, ds, mask)
